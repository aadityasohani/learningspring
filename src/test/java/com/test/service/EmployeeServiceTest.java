package com.test.service;

import com.test.model.Employee;
import com.test.repository.EmployeeRepository;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
public class EmployeeServiceTest {
    private String cityName = "bhopal";
    @Mock
    EmployeeRepository employeeRepository;
    @InjectMocks
    EmployeeService employeeService;
    @Test
    public void testGetEmployee() {
        Employee emp1 = new Employee("1", "aaa", 24, "bhopal");
//        Employee emp2 = new Employee("2", "bbb", 22, "indore");
        Employee emp3 = new Employee("3", "ccc", 21, "bhopal");
//        Employee emp4 = new Employee("4", "ddd", 26, "khandwa");
        Employee emp5 = new Employee("5", "eee", 28, "bhopal");
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(emp1);
//        employeeList.add(emp2);
        employeeList.add(emp3);
//        employeeList.add(emp4);
        employeeList.add(emp5);
        Mockito.when(employeeRepository.getEmployeeByCity(cityName)).thenReturn(employeeList);
        double res = employeeService.getAverageAgeByCity(cityName);
        Assertions.assertEquals(24.3,res);
        System.out.println(res);

    }
}
