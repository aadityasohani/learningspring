package com.test.service;

//import org.junit.Assert;
//import org.junit.Test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestDemo {
    @Test
    public void testSum(){
        Demo demo = new Demo();
        int res = demo.add(2,3);
        Assertions.assertEquals(5,res);
    }
}
