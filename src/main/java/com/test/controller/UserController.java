package com.test.controller;


import com.test.model.Userspring;
import com.test.repository.UserRepository;
import com.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public List<Userspring> findAllUsers(){
        return userService.findALLUsers();
    }
    @GetMapping("/user/{uname}")
    public Userspring findUsersWithName(@PathVariable String uname){

        return userService.findByUsName(uname);
    }

}
