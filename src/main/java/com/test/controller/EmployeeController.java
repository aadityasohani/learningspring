package com.test.controller;

import com.test.model.Employee;
import com.test.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    DiscoveryClient client;

    @GetMapping("employee/{city}")
    public List<Employee> getEmployeeByCityName(@PathVariable String city){
        return employeeService.getEmployeeByCityName(city);
    }
    @GetMapping("/employee")
    public List<Employee> getEntity(){
        List<Employee> x = employeeService.getEmployee();
        return x;
    }

    @GetMapping("/getAvg/{city}")
    public double getAvgAgeByCity(@PathVariable String city){
        return employeeService.getAverageAgeByCity(city);
    }
    @GetMapping("/getSalary/{id}")
    public long getSalaryById(@PathVariable String id){
        int age = employeeService.getEmployeeAgeById(id);
        RestTemplate restTemplate = new RestTemplate();

//        long ans = restTemplate.getForObject("http://localhost:8081/getSalary/"+age,Long.class);

        URI uri = client.getInstances("salary-data-provider").stream().map(si->si.getUri()).findFirst()
                .map(s->s.resolve("/getSalary/"+age)).get();

        long ans = restTemplate.getForObject(uri,Long.class);

        return ans;

    }
    @PostMapping("/addEmp")
    public String saveEmployee(@RequestBody Employee e){
        return employeeService.setEmployee(e);
    }

    @DeleteMapping("/deleteEmp")
    public String deleteEmployee(@RequestBody String s){
        return employeeService.deleteEmployee(s);
    }

    @PostMapping("/addEmps")
    public List<String> addMultiple(@RequestBody List<Employee>eList){
        return employeeService.addMultipleEmployees(eList);
    }

    @PutMapping("/update/{id}")
    public String updateEmployee(@RequestBody Employee e, @PathVariable String id){
        return employeeService.updateEntry(e,id);
    }

}
