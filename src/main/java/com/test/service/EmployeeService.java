package com.test.service;

import com.test.model.Employee;
import com.test.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;



    public List<Employee> getEmployee(){
        return employeeRepository.findAll();
    }

    public List<Employee> getEmployeeByCityName(String city){
        return employeeRepository.getEmployeeByCity(city);
    }
    public int getEmployeeAgeById(String id){
        Employee e = employeeRepository.findEmployeeById(id);
        return e.getAge();
    }

    public double getAverageAgeByCity(String city){
        List<Employee> employees = employeeRepository.getEmployeeByCity(city);
        int ans = 0;
        int count = 0;
        for(Employee e : employees){
            ans += e.getAge();
        }
        count = employees.size();
//        System.out.println(ans+" "+count);

        return Math.round(((double)ans/(double)count)*10)/10.0;
    }

    public String setEmployee(Employee e){
        if(employeeRepository.existsById(e.getId())) {
            return "Employee id already exists";
        }else{
            employeeRepository.save(e);
            return "data added successfully";
        }
    }

    public String deleteEmployee(String e){
        if(!employeeRepository.existsById(e)){
            return "employee not found";
        }else{
            employeeRepository.deleteById(e);
            return "deleted successfully";
        }
    }


    public List<String> addMultipleEmployees(List<Employee> eList){
        List<String> sList = new ArrayList<>();
        for(Employee e :eList){
            sList.add(setEmployee(e));
        }

        return sList;
    }

    public String updateEntry(Employee e,String id){

            employeeRepository.findById(id).map(employee->{
                employee.setName(e.getName());
                employee.setAge(e.getAge());
                employeeRepository.save(employee);
                return "updated successfully";
            }).orElseGet(()->{
                return "no user found";
            });
        return "updated successfully";
    }
}
