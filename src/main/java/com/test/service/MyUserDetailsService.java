package com.test.service;

import com.test.model.Userspring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class MyUserDetailsService  implements UserDetailsService {
    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Userspring userspring = userService.findByUsName(username);
        if(userspring == null){
            throw new UsernameNotFoundException("404 user not found");
        }
        return new UserPrincipal(userspring);
    }
}
