package com.test.service;
import com.test.model.Userspring;
import com.test.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;


    public List<Userspring> findALLUsers(){
        return userRepository.findAll();
    }

    public Userspring findByUsName(String uName){
        return userRepository.findByUsername(uName);
    }


}
